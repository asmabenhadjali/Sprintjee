package Services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.Entity.Competence;
import tn.esprit.Entity.Employe;
import tn.esprit.Entity.Niveau;





@Remote
public interface CompetenceRemote {
	
	public void addCompetence(Competence c);
	public void deleteCompetence(int id);
	public Competence findCompetence(int id);
	public void updateCompetence(Competence c);
	public List<Competence> getAllCompetence();
	public List<Competence> getAllCompetences(int id);
	public List<Employe> getListEmployes() ;
	public List<Niveau> getAllNiveau();
	public List<Niveau> getNiveauByid(int id);
	public void addNiveau(Niveau entreprise);
	public void updateNiveau(Niveau c);
	
	public long getNbEmploye(int id) ;
}
