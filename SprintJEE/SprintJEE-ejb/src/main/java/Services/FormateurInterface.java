package Services;

import java.util.List;

import tn.esprit.Entity.Formateur;



public interface FormateurInterface {
	Formateur findByFormateurById(int Id);
	List<Formateur> findAllFormateurs();
	void addFormateur(Formateur formateur);

	void deleteFormateur(Formateur formateur);
	public void updateFormateur() ;
	//public void updateFormateur(Formateur formateur);
	void updateFormateur(Formateur formateur);
}
