package Services;

import java.util.Date;
import java.util.List;

import tn.esprit.Entity.Formateur;
import tn.esprit.Entity.Formation;




public interface PlanificationInterface {
	public int ajouterFormation(Formation formation);
	public void affecterFormationAFormateur(int FormationId, int Formateurid);
	public void ajouterPlanification(int FormationId, int FormateurId, Date dateDebut, Date dateFin,  int numberP);
	//public void validerPlanification(int FormationId, int FormateurId, Date dateDebut, Date dateFin, int numberP, validateurId);
	public List<Formation> findAllFormationByFormateurJPQL(int FormateurID);
	public List<Formateur> getAllFormateurByFormation(int FormationId);

}
