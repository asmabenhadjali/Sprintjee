package Services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.esprit.Entity.Employe;
import tn.esprit.Entity.Evaluation;






@Stateless
@LocalBean
public class EvaluationSheetService  {

	@PersistenceContext
	EntityManager em;


	public int ajouterEvaluationSheet(Evaluation evaluationsheet) {
		em.persist(evaluationsheet);
		return evaluationsheet.getId();
	}
	
	public void updateEval(Evaluation evaluationsheet) { 

		em.merge(evaluationsheet);
	}

	
	public Evaluation findEvalById(int id) {
		
		Evaluation eval = em.find(Evaluation.class, id);
		
		return eval;
	}
	public List <Employe> getAllEmployes() { List <Employe> emp = em.createQuery("Select e from Employe e",
			Employe.class).getResultList(); return emp;} 
	
	public void removeEval(int id) {
		System.out.println("In removeEmp : ");
		em.remove(em.find(Evaluation.class, id));
		System.out.println("Out of removeEmp : ");	

	}
	public void deleteAllevaluationJPQL() {
		int modified = em.createNativeQuery("DELETE from evaluationsheet WHERE DATEDIFF(NOW(),date) >7 ").executeUpdate();
		if(modified > 1){
			System.out.println("successfully deleted");
		}else{
			System.out.println("failed to delete");
		}
	}
	public List <Evaluation> getAllEval() { List <Evaluation> emp = em.createQuery("Select e from EvaluationSheet e",
			Evaluation.class).getResultList(); return emp;}
	
	
	public Employe findEemp(int id) {
		
		Employe user = em.find(Employe.class, id);
		
		return user;
	}
	
	
	}