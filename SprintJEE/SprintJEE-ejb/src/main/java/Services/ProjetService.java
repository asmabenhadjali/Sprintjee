package Services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import tn.esprit.Entity.Projet;

 
@Stateless
@LocalBean
public class ProjetService implements ProjetServiceRemote {
	@PersistenceContext
	EntityManager em;

	@Override
	public void AddProjet(Projet p) {
		em.persist(p);
		
	}

	@Override
	public List<Projet> getAllprojets() {
		List<Projet> result = em.createQuery("from Projet", Projet.class).getResultList();
		
		return result;
	}

	@Override
	public Projet getProjetById(int id) {
		return em.find(Projet.class, id);
	}

	@Override
	public void deleteProjetById(int id) {
		em.createQuery("delete From Projet e where e.id=:id").setParameter("id", id).executeUpdate();
		
	}

	@Override
	public void updateProjet(Projet t) {
		em.merge(t);
		em.flush();
		
	}

	
	
	
	

}
