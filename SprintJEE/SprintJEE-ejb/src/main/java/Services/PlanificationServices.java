package Services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.Entity.Formateur;
import tn.esprit.Entity.Formation;
import tn.esprit.Entity.Planification;





public class PlanificationServices  implements PlanificationInterface {
	 
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public int ajouterFormation(Formation formation) {
		entityManager.persist(formation);
		return formation.getId();
	}



	@Override
	public void affecterFormationAFormateur(int FormationId, int Formateurid) {
		Formation formation = entityManager.find(Formation.class, FormationId);
		Formateur form = entityManager.find(Formateur.class, Formateurid);
		//formation.setFormation(formation);
		entityManager.merge(formation);
	}

	@Override
	public void ajouterPlanification(int FormationId, int FormateurId, Date dateDebut, Date dateFin, int numberP) {
		Planification planification = new Planification();
		Planification.setDateDebut(dateDebut);
		Planification.setDateFin(dateFin);
		//Planification.setFormation(FormateurId);
		//Planification.setFormation(FormationId);
	//	Planification.setValide(false); //par defaut non valide
		entityManager.persist(planification);
	}
		

	//@Override
	//public void validerPlanification(int FormationId, int FormateurId, Date dateDebut, Date dateFin, int numberP, int validateurId) {
		//Formateur validateur = entityManager.find(Formateur.class, validateurId);
		//Mission mission = entityManager.find(Mission.class, missionId);
		
		//verifier s'il est un chef de departement (interet des enum)
		//if(!validateur.getRole().equals(Role.CHEF_DEPARTEMENT)){
			//System.out.println("l'employe doit etre chef de departement pour valider une feuille de temps !");
			//return;
		//}
		//verifier s'il est le chef de departement de la mission en question
		//boolean chefDeLaMission = false;
		//for(Departement dep : validateur.getDepartements()){
			//if(dep.getId() == mission.getDepartement().getId()){
				//chefDeLaMission = true;
				//break;
			//}
		//}
		//if(!chefDeLaMission){
			//System.out.println("l'employe doit etre chef de departement de la mission en question");
			//return;
		//}

		
	//}

	@Override
	public List<Formation> findAllFormationByFormateurJPQL(int FormateurID) {
		TypedQuery<Formation> query= entityManager.createQuery("select DISTINCT m from Formation f join f.planifications p join p.formateur where Formtr.id=:FormateurId", 
				Formation.class);
				query.setParameter("formateurId", FormateurID);
				return query.getResultList();
	}

	@Override
	public List<Formateur> getAllFormateurByFormation(int formationId) {
		TypedQuery<Formateur> query= entityManager.createQuery(
				"select DISTINCT f from Formateur f "
				+ "join f.Planification p "
				+ "join p.formation  F "
				+ "where F.id=:FormationId", 
				Formateur.class);
				
				query.setParameter("FormationId", formationId);
				
				return query.getResultList();
	}
	
//	for(Employe employe : employes){
//		System.out.println("emp ID : " + employe.getId());
//	}

}
