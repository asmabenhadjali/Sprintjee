package Services;


import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import tn.esprit.Entity.Competence;
import tn.esprit.Entity .Employe;
import tn.esprit.Entity.Niveau;




@Stateless
@LocalBean
public class CompetenceService implements CompetenceRemote, CompetenceLocal {
	
	
	@PersistenceContext
    EntityManager em; 
	
 
	@Override
	public void addCompetence(Competence entreprise) {
		em.persist(entreprise);
		
	}
	@Override
	public void addNiveau(Niveau entreprise) {
		em.persist(entreprise);
		
	}

	@Override
	public void deleteCompetence(int id) {
		em.remove(em.find(Competence.class, id ));
		
	}

	@Override
	public Competence findCompetence(int id) {
		return em.find(Competence.class, id );
	}

	@Override
	public void updateCompetence(Competence entreprise) {
		em.merge(entreprise);
	}
	@Override
	public List<Competence> getAllCompetence() {
		List<Competence> emp = em.createQuery("Select e from Competence e",Competence.class).getResultList();
		return emp;
		}
	@Override
	public List<Competence> getAllCompetences(int id) {
		List<Competence> lstfinal= new ArrayList<Competence>();
		List<Competence> emp = em.createQuery("Select e from Competence e",Competence.class).getResultList();
		List<Niveau> lstniv = em.createQuery("Select e from Niveau e ",Niveau.class).getResultList();
		for(int i=0; i<emp.size(); i++){
			for(int j=0; j<lstniv.size(); j++) {
				if((emp.get(i).getId()==lstniv.get(j).getCompetence().getId())&&(lstniv.get(j).getEmploye().getId()==id))
				{
					i++;
					j=lstniv.size()+1;
				}else
				lstfinal.add(emp.get(i));
				i++;
				j=lstniv.size()+1;
			}
			
		}
		
		return lstfinal;
		}
	@Override
	public List<Employe> getListEmployes() {
		TypedQuery<Employe> query = em.createQuery("Select e from Employe e", Employe.class);
		List<Employe> result = query.getResultList();
		return result;
	}
	public List<Niveau> getAllNiveau(){
		List<Niveau> emp = em.createQuery("Select e from Niveau e ",Niveau.class).getResultList();
		return emp;
	}
	@Override
	public List<Niveau> getNiveauByid(int id) {
		TypedQuery<Niveau> query = em.createQuery("Select e from Niveau e where e.employe.id=:id", Niveau.class);
		List<Niveau> emp= query.setParameter("id", id).getResultList();
		return  emp;
	}
	public void updateNiveau(Niveau c) {
		em.merge(c);
	}
	@Override
	public long getNbEmploye(int id) {
		TypedQuery<Long> query = em.createQuery("Select COUNT(*) FROM Niveau e where e.competence.id=:id", Long.class);
		query.setParameter("id", id);
		long EmployeCount = query.getSingleResult();
		return EmployeCount;

	}
}
