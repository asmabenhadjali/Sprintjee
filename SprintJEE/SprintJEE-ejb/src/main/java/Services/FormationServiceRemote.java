package Services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.Entity.Formateur;
import tn.esprit.Entity.Formation;



@Remote
public interface FormationServiceRemote  {
	Formation findByFormationById(int Id);
	List<Formation> findAllFormation();
	void addFormation(Formation formation);
	void updateFormation(Formation formation);
	void deleteFormation(Formation formation);
	public void updateFormateur(Formateur fo);

}
