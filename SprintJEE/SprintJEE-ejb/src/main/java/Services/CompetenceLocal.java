package Services;

import javax.ejb.Local;

import tn.esprit.Entity.Competence;

@Local
public interface CompetenceLocal {
	public void addCompetence(Competence entreprise);
	public void deleteCompetence(int id);
	public Competence findCompetence(int id);
	public void updateCompetence(Competence entreprise);

}
