package Services;

import java.util.List;

import javax.ejb.Remote;

import tn.esprit.Entity.Projet;



@Remote
public interface ProjetServiceRemote {
	
	void AddProjet(Projet p);

	List<Projet> getAllprojets();
	public Projet getProjetById(int id);

	
	public void deleteProjetById(int id);

	void updateProjet(Projet t);
}
