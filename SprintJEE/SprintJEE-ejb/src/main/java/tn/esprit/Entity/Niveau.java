package tn.esprit.Entity;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;




@Entity
public class Niveau implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private NiveauPK niveaupk;
	
	@ManyToOne
	@JoinColumn(name="idEmploye",referencedColumnName="id",insertable=false , updatable=false)
	private Employe employe;
	
	@ManyToOne
	@JoinColumn(name="idCompetence",referencedColumnName="id",insertable=false , updatable=false)
	private Competence competence;
	
	private int num_niveau;

	
	
	public Niveau() {
		super();
	}

	public NiveauPK getNiveaupk() {
		return niveaupk;
	}

	public void setNiveaupk(NiveauPK niveaupk) {
		this.niveaupk = niveaupk;
	}

	public Employe getEmploye() {
		return employe;
	}

	public void setEmploye(Employe employe) {
		this.employe = employe;
	}

	public Competence getCompetence() {
		return competence;
	}

	public void setCompetence(Competence competence) {
		this.competence = competence;
	}

	public int getNum_niveau() {
		return num_niveau;
	}

	public void setNum_niveau(int num_niveau) {
		this.num_niveau = num_niveau;
	}

	public Niveau(NiveauPK niveaupk) {
		super();
		this.niveaupk = niveaupk;
	}
	
	
	
	
	
}
