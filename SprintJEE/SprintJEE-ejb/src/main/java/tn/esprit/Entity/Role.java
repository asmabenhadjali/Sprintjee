package tn.esprit.Entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Role implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id ;

	@OneToOne(mappedBy="role")	
	private Employe Employe;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
 
	

}
