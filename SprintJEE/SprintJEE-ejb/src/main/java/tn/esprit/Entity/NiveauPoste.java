package tn.esprit.Entity;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class NiveauPoste implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PostePK postepk;
	
	@ManyToOne
	@JoinColumn(name="idPoste",referencedColumnName="id",insertable=false , updatable=false)
	private Poste poste;
	
	public NiveauPoste() {
		super();
	}

	public NiveauPoste(PostePK postepk) {
		super();
		this.postepk = postepk;
	}

	@ManyToOne
	@JoinColumn(name="idCompetence",referencedColumnName="id",insertable=false , updatable=false)
	private Competence competence;
	
	private int num_niveau;

	public PostePK getPostepk() {
		return postepk;
	}

	public void setPostepk(PostePK postepk) {
		this.postepk = postepk;
	}

	public Poste getPoste() {
		return poste;
	}

	public void setPoste(Poste poste) {
		this.poste = poste;
	}

	public Competence getCompetence() {
		return competence;
	}

	public void setCompetence(Competence competence) {
		this.competence = competence;
	}

	public int getNum_niveau() {
		return num_niveau;
	}

	public void setNum_niveau(int num_niveau) {
		this.num_niveau = num_niveau;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	

}
