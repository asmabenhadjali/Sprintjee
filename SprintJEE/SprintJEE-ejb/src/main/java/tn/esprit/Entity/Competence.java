package tn.esprit.Entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;



@Entity
public class Competence implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Competence() {
		super();
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id ;
	
	private String Description;
	
	private TypeCompetence typeCompetence;

	@OneToMany(mappedBy="competence")
	private List<Niveau> niveau;
	
	@OneToMany(mappedBy="competence")
	private List<NiveauPoste> niveauP;
	
	public Competence(String description2, TypeCompetence typeCompetence2) {
		Description=description2;
		typeCompetence = typeCompetence2;
	}

	public Competence(String descr) {
		Description=descr;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public List<Niveau> getNiveau() {
		return niveau;
	}

	public TypeCompetence getTypeCompetence() {
		return typeCompetence;
	}

	public void setTypeCompetence(TypeCompetence typeCompetence) {
		this.typeCompetence = typeCompetence;
	}

	public void setNiveau(List<Niveau> niveau) {
		this.niveau = niveau;
	}


	

}
