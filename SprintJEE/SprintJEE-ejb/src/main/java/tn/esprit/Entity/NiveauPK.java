package tn.esprit.Entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class NiveauPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idEmploye; 
	private int idCompetence;
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idCompetence;
		result = prime * result + idEmploye;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NiveauPK other = (NiveauPK) obj;
		if (idCompetence != other.idCompetence)
			return false;
		if (idEmploye != other.idEmploye)
			return false;
		return true;
	}
	public int getIdEmploye() {
		return idEmploye;
	}
	public void setIdEmploye(int idEmploye) {
		this.idEmploye = idEmploye;
	}
	public int getIdCompetence() {
		return idCompetence;
	}
	public void setIdCompetence(int idCompetence) {
		this.idCompetence = idCompetence;
	}
	public NiveauPK(int idEmploye, int idCompetence) {
		super();
		this.idEmploye = idEmploye;
		this.idCompetence = idCompetence;
	}
	
	public NiveauPK() {
		
	}
	
	
	
}
