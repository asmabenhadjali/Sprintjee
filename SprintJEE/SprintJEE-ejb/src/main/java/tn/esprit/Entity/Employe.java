package tn.esprit.Entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;



@Entity
public class Employe implements Serializable{
	

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id ; 
	private String nom ; 
	private String prenom;
	private String email;
	
	
	@OneToMany(mappedBy="employe")
	private List<Niveau> niveau;
	
	@ManyToMany
	private List<Mission> missions;
	
	@OneToMany(mappedBy="EvalEmploye")
	private List<Evaluation> Evaluation;
	
	@OneToOne
	private Role role;
	
	
	@OneToMany
	private List<Abscence> abscence;

	
	@OneToMany(cascade=CascadeType.ALL ,mappedBy="employe")
	private List<Tache> taches;
	
	

	
	
	
	
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public int getId() {
		return id;
	}
	public List<Abscence> getAbscence() {
		return abscence;
	}
	public void setAbscence(List<Abscence> abscence) {
		this.abscence = abscence;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getEmmail() {
		return email;
	}
	public void setEmmail(String emmail) {
		this.email = emmail;
	}

	
	

}
