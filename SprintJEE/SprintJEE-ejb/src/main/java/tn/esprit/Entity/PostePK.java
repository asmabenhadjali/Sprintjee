package tn.esprit.Entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class PostePK implements Serializable  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idPoste; 
	private int idCompetence;
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idCompetence;
		result = prime * result + idPoste;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PostePK other = (PostePK) obj;
		if (idCompetence != other.idCompetence)
			return false;
		if (idPoste != other.idPoste)
			return false;
		return true;
	}
	public PostePK(int idPoste, int idCompetence) {
		super();
		this.idPoste = idPoste;
		this.idCompetence = idCompetence;
	}
	public PostePK() {
		
	}
	
	

}
