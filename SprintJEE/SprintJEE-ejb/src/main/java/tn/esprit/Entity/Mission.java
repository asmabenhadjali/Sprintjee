package tn.esprit.Entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Mission implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idM;
	
	@ManyToMany	
	private List<Employe> Employees;
	
	private String nomM;
	private String descriptionM;
	private String typeM;
	private String zoneM;
	private Date dateDebutM;
	private String statut;
	private String Duree;
	private float plafond;
	private float consommation;
	@OneToMany
	private List<FraiMission> listConso;



public List<Employe> getEmployees() {
	return Employees;
}
public void setEmployees(List<Employe> employees) {
	Employees = employees;
}
public int getIdM() {
	return idM;
}
public void setIdM(int idM) {
	this.idM = idM;
}
public String getNomM() {
	return nomM;
}
public void setNomM(String nomM) {
	this.nomM = nomM;
}
public String getDescriptionM() {
	return descriptionM;
}
public void setDescriptionM(String descriptionM) {
	this.descriptionM = descriptionM;
}
public String getTypeM() {
	return typeM;
}
public void setTypeM(String typeM) {
	this.typeM = typeM;
}
public String getZoneM() {
	return zoneM;
}
public void setZoneM(String zoneM) {
	this.zoneM = zoneM;
}

/*@Temporal (TemporalType.TIMESTAMP)
public Date getDateDebutM() {
	return dateDebutM;
}*/
public void setDateDebutM(Date dateDebutM) {
	this.dateDebutM = dateDebutM;
}
public String getStatut() {
	return statut;
}
public void setStatut(String statut) {
	this.statut = statut;
}
public String getDuree() {
	return Duree;
}
public void setDuree(String duree) {
	Duree = duree;
}
public float getPlafond() {
	return plafond;
}
public void setPlafond(float plafond) {
	this.plafond = plafond;
}
public float getConsommation() {
	return consommation;
}
public void setConsommation(float consommation) {
	this.consommation = consommation;
}




}
