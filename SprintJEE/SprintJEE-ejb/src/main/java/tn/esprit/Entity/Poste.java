package tn.esprit.Entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Poste implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id ;
	private String description;
	private int salaire;
	
	public Poste(String description, int salaire) {
		super();
		this.description = description;
		this.salaire = salaire;
	}
	@OneToMany(mappedBy="poste")
	private List<NiveauPoste> niveauP;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getSalaire() {
		return salaire;
	}
	public void setSalaire(int salaire) {
		this.salaire = salaire;
	}
	public List<NiveauPoste> getNiveauP() {
		return niveauP;
	}
	public void setNiveauP(List<NiveauPoste> niveauP) {
		this.niveauP = niveauP;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Poste() {
		super();
	}
	
	
	
	
	
	

}
