package tn.esprit.Entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Projet implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
	
	private String name;
	private String description;
	private Date start_date;
	private Date end_date;
	
	private float budget;
	@OneToMany(mappedBy="projet")
	List<Tache> taches;
	
	
	




public Projet(String name, String description, Date start_date, Date end_date, float budget) {
		super();
		this.name = name;
		this.description = description;
		this.start_date = start_date;
		this.end_date = end_date;
		
		this.budget = budget;
	}



public Projet() {
		super();
	}



public int getId() {
	return id;
}



public void setId(int id) {
	this.id = id;
}



public String getName() {
	return name;
}



public void setName(String name) {
	this.name = name;
}



public String getDescription() {
	return description;
}



public void setDescription(String description) {
	this.description = description;
}



public Date getStart_date() {
	return start_date;
}



public void setStart_date(Date start_date) {
	this.start_date = start_date;
}



public Date getEnd_date() {
	return end_date;
}



public void setEnd_date(Date end_date) {
	this.end_date = end_date;
}



public float getBudget() {
	return budget;
}



public void setBudget(float budget) {
	this.budget = budget;
}



public List<Tache> getTaches() {
	return taches;
}



public void setTaches(List<Tache> taches) {
	this.taches = taches;
}



public static long getSerialversionuid() {
	return serialVersionUID;
}






}
