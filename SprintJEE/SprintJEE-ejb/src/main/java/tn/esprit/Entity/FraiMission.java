package tn.esprit.Entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
@Entity
public class FraiMission implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idR;
	
	private String nomFM;
	private String descriptionFM;
	private String typeFM;
	private String urlFM;
	private Date dateFM;
	private Float depense;
	private String necessite;
	
	private Mission mission;

	public int getIdR() {
		return idR;
	}

	public void setIdR(int idR) {
		this.idR = idR;
	}

	public String getNomFM() {
		return nomFM;
	}

	public void setNomFM(String nomFM) {
		this.nomFM = nomFM;
	}

	public String getDescriptionFM() {
		return descriptionFM;
	}

	public void setDescriptionFM(String descriptionFM) {
		this.descriptionFM = descriptionFM;
	}

	public String getTypeFM() {
		return typeFM;
	}

	public void setTypeFM(String typeFM) {
		this.typeFM = typeFM;
	}

	public String getUrlFM() {
		return urlFM;
	}

	public void setUrlFM(String urlFM) {
		this.urlFM = urlFM;
	}
	/*@Temporal (TemporalType.TIMESTAMP)
	public Date getDateFM() {
		return dateFM;
	}
*/
	public void setDateFM(Date dateFM) {
		this.dateFM = dateFM;
	}

	public Float getDepense() {
		return depense;
	}

	public void setDepense(Float depense) {
		this.depense = depense;
	}

	public String getNecessite() {
		return necessite;
	}

	public void setNecessite(String necessite) {
		this.necessite = necessite;
	}
	@ManyToOne
	public Mission getMission() {
		return mission;
	}

	public void setMission(Mission mission) {
		this.mission = mission;
	}
	
	

}
