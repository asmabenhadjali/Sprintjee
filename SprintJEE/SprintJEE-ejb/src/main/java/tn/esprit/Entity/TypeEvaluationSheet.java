package tn.esprit.Entity;

public enum TypeEvaluationSheet {
	 EvaluationByEmploye, EvaluationByManager, SelfEvaluation
}