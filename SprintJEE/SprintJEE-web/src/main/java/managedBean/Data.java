package managedBean;

import java.io.Serializable;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import tn.esprit.Entity.State;
import tn.esprit.Entity.TypeEvaluationSheet;


@ManagedBean
@ApplicationScoped
public class Data implements Serializable {
	private static final long serialVersionUID = 1L;
	 
	private String name ;
	public State[] getState()
	{
		return State.values();
		}
	public TypeEvaluationSheet[] getType() {
		return TypeEvaluationSheet.values();
	}
	
	
}
