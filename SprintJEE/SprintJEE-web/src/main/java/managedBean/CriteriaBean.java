package managedBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import Services.CriteriaService;
import tn.esprit.Entity.Criteria;
import tn.esprit.Entity.Evaluation;
import tn.esprit.Entity.TypeEvaluationSheet;

@ManagedBean(name = "criteriaBean") 
@SessionScoped 
public class CriteriaBean  implements Serializable{
	private static long serialVersionUID = 1L;
	
	private List<Criteria> criterias;
	private TypeEvaluationSheet typeevaluation;
	public TypeEvaluationSheet getTypeevaluation() {
		return typeevaluation;
	}



	public void setTypeevaluation(TypeEvaluationSheet typeevaluation) {
		this.typeevaluation = typeevaluation;
	}



	public Date getDate() {
		return date;
	}



	public void setDate(Date date) {
		this.date = date;
	}

	private Date date;
	
	public CriteriaBean() {
		criterias = new ArrayList<>();
	}
	
	

	public List<Criteria> getCriterias() {
		//criterias = criteriaservice.getAllCriterien();
		return criterias;
	}
	public void RemoveCriterien(int criterienId) { criterias.remove(criterienId);}

	@EJB 
	CriteriaService criteriaservice;
public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

private String description;
	

	public void addECriterien() { 
		//criteriaservice.ajouterCriteria((new Criteria(description)));
		criterias.add(new Criteria(description));
		
		
	}
	
	public String addEv() {
		Evaluation ev = new Evaluation();
		ev.setCriteria(criterias);
		for(Criteria criteria : criterias) {
			criteria.setEvaluationsheet(ev);
		}
		ev.setTypeevaluation(typeevaluation);
		ev.setDate(date);
		int id = criteriaservice.ajouterev(ev);
		System.out.println(typeevaluation);
		criterias.clear();
		return "evaluationForm.xhtml/?faces-redirect=true&sheetID="+ id; 
	}
	
	public String addForm() {
		return "FormulaireCritere.xhtml/?faces-redirect=true"; 
	}
	
	
	

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public static void setSerialversionuid(long serialversionuid) {
		serialVersionUID = serialversionuid;
	}

	public CriteriaService getCriteriaservice() {
		return criteriaservice;
	}

	public void setCriteriaservice(CriteriaService criteriaservice) {
		this.criteriaservice = criteriaservice;
	}

	public String getNew_criteria() {
		return new_criteria;
	}

	public void setNew_criteria(String new_criteria) {
		this.new_criteria = new_criteria;
	}

	private String new_criteria;	
	
	



	public void setCriterias(List<Criteria> criterias) {
		this.criterias = criterias;
	}

	public String getNewCriteria() {
		return new_criteria;
	}



	public void setNewCriteria(String new_criteria) {
		this.new_criteria = new_criteria;
	}
	

}
