package managedBean;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import Services.CompetenceService;
import tn.esprit.Entity.Competence;
import tn.esprit.Entity.Niveau;


import tn.esprit.Entity.TypeCompetence;


@ManagedBean
@SessionScoped
public class CompetenceBean {

	@EJB
	CompetenceService servicecompetence;
	
	private String Description; 
	private TypeCompetence typeCompetence;
	private Competence competence = new Competence();
	private Competence selectedCompetence = new Competence();
	private List<Competence> competences;
	private Niveau nv= new Niveau();
	
	private List<Competence>filteredCars;
	
	
	public List<Competence> getFilteredCars() {
		return filteredCars;
	}
	public void setFilteredCars(List<Competence> filteredCars) {
		this.filteredCars = filteredCars;
	}
	public List<Competence> getCompetences() {
		competences = servicecompetence.getAllCompetence();
		return competences;
	}
	public void setCompetences(List<Competence> competences) {
		this.competences = competences;
	}
	public void addCompet(){
		servicecompetence.addCompetence(new Competence(Description,typeCompetence));
		
		}
	public void updateCompetes(){
		servicecompetence.updateCompetence(selectedCompetence);
		}
	public void delCompetes(int id){
		servicecompetence.deleteCompetence(id);
		}
	
	
	public Niveau getNv() {
		return nv;
	}
	public void setNv(Niveau nv) {
		this.nv = nv;
	}
	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public TypeCompetence getTypeCompetence() {
		return typeCompetence;
	}

	public void setTypeCompetence(TypeCompetence typeCompetence) {
		this.typeCompetence = typeCompetence;
	}

	
	
	public CompetenceService getServicecompetence() {
		return servicecompetence;
	}
	public void setServicecompetence(CompetenceService servicecompetence) {
		this.servicecompetence = servicecompetence;
	}
	public Competence getSelectedCompetence() {
		return selectedCompetence;
	}

	public void setSelectedCompetence(Competence selectedCompetence) {
		this.selectedCompetence = selectedCompetence;
	}

	
	
	public Competence getCompetence() {
		return competence;
	}

	public void setCompetence(Competence competence) {
		this.competence = competence;
	}
	
	

	
	
}
