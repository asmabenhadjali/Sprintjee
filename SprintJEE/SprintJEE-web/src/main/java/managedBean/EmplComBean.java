package managedBean;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import Services.CompetenceService;
import tn.esprit.Entity.Competence;
import tn.esprit.Entity.Employe;
import tn.esprit.Entity.Niveau;
import tn.esprit.Entity.NiveauPK;


@ManagedBean
@SessionScoped
public class EmplComBean {
	
	private String prenom; 
	private String nom;
	private String password;
	private String email;
	private List<Employe> employes;
	private List<Competence> competences;
	private List<Niveau> niveaux;
	private List<Niveau> niveauEmploye;
	private Employe selected= new Employe();
	private Niveau niveau;
	private Niveau selectedniveau= new Niveau();
	private NiveauPK nvp;
	private int num;
	private int id;
	private List<Niveau>filteredCompetence;

	@EJB
	CompetenceService servicecompetence;
	
	
	
	
	public Employe getSelected() {
		return selected;
	}

	public void setSelected(Employe selected) {
		this.selected = selected;
	}

	public List<Niveau> getNiveaux() {
		niveaux = servicecompetence.getNiveauByid(selected.getId());
		return niveaux;
	}

	public void setNiveaux(List<Niveau> niveaux) {
		this.niveaux = niveaux;
	}

	public List<Employe> getEmployes() {
		employes = servicecompetence.getListEmployes();
		return employes;
		}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public CompetenceService getServicecompetence() {
		return servicecompetence;
	}

	public void setServicecompetence(CompetenceService servicecompetence) {
		this.servicecompetence = servicecompetence;
	}

	public void setEmployes(List<Employe> employes) {
		this.employes = employes;
	}

	public List<Competence> getCompetences() {
		competences = servicecompetence.getAllCompetence();
		return competences;
	}

	public void setCompetences(List<Competence> competences) {
		this.competences = competences;
	}

	public Niveau getNiveau() {
		return niveau;
	}

	public void setNiveau(Niveau niveau) {
		this.niveau = niveau;
	}
	
	public NiveauPK getNvp() {
		return nvp;
	}

	public void setNvp(NiveauPK nvp) {
		this.nvp = nvp;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void addNiv(){
		nvp = new NiveauPK(selected.getId(), id);
		niveau= new Niveau(nvp);
		niveau.setNum_niveau(num);
		servicecompetence.addNiveau(niveau);
		}

	public Niveau getSelectedniveau() {
		return selectedniveau;
	}

	public void setSelectedniveau(Niveau selectedniveau) {
		this.selectedniveau = selectedniveau;
	}
	public void updateNiv(){
	
		servicecompetence.updateNiveau(selectedniveau);
		}

	public List<Niveau> getFilteredCompetence() {
		return filteredCompetence;
	}

	public void setFilteredCompetence(List<Niveau> filteredCompetence) {
		this.filteredCompetence = filteredCompetence;
	}

	public List<Niveau> getNiveauEmploye() {
		niveauEmploye= servicecompetence.getNiveauByid(2);
		return niveauEmploye;
	}

	public void setNiveauEmploye(List<Niveau> niveauEmploye) {
		this.niveauEmploye = niveauEmploye;
	}


	
	
}
