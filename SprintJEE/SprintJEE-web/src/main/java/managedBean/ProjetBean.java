package managedBean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import Services.ProjetService;
import tn.esprit.Entity.Projet;


@ManagedBean
@SessionScoped
public class ProjetBean  implements Serializable  {
	
	private static final long serialVersionUID = 1L;
	
	@EJB
	ProjetService projetService;
	
	
	private String name;
	private String description;
	private Date start_date;
	private Date end_date;

	private float budget;
	private int idToBeUpdate ;
	                                                                                           
	private List<Projet> projets;
	
	
	

	
	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	public List<Projet> getProjets() {
		return projets = projetService.getAllprojets();
	}



	public void ajouterProjet() {
		

		Projet projet=new Projet();
	
		projet.setBudget(budget);
		projet.setDescription(description);
		projet.setEnd_date(end_date);
		projet.setStart_date(start_date);
		projet.setName(name);
		projetService.AddProjet(projet);
		initialisation();
	}
	
	
	
	public void supprimerProjet(int id) {
		
		projetService.deleteProjetById(id);
	}

	public void recupererProjet(Projet e) {
		initialisation();
        name=e.getName();
        description=e.getDescription();
        budget=e.getBudget();
        
        end_date=e.getEnd_date();
        start_date=e.getStart_date();
		this.setIdToBeUpdate(e.getId());

	}

	public void updateProjet() {
		Projet ee = new Projet();

		ee.setId(this.getIdToBeUpdate());
		ee.setDescription(description);
		ee.setName(name);
	ee.setEnd_date(end_date);
	ee.setStart_date(start_date);
	ee.setBudget(budget);
		
	projetService.updateProjet(ee);
		initialisation();
	}
	
	public void setProjets(List<Projet> projets) {
		this.projets = projets;
	}

	public void initialisation() {
		name = null;
		description = null;
	budget=(float) 0.0;
	end_date=new Date();
	start_date=new Date();
	}
	
	
	
	
	public int getIdToBeUpdate() {
		return idToBeUpdate;
	}

	public void setIdToBeUpdate(int idToBeUpdate) {
		this.idToBeUpdate = idToBeUpdate;
	}

	



	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	

	public float getBudget() {
		return budget;
	}

	public void setBudget(float budget) {
		this.budget = budget;
	}


	
	

	
}
